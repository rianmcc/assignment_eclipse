package herovillain;

/**
 * Crime is the class for creating the different crimes that can occur during
 * the game. Each crime can apply to hero's or villain's.
 */
public class Crime {

	private String shortDescription;
	private int energyUsage, actionPoints, ignorePoints, moneyReward;

	public Crime(String shortDescription, int energyUsage, int actionPoints,
			int ignorePoints, int moneyReward) {
		this.shortDescription = shortDescription;
		this.energyUsage = energyUsage;
		this.actionPoints = actionPoints;
		this.ignorePoints = ignorePoints;
		this.moneyReward = moneyReward;
	}

	public void setEnergyUsage(int energyUsage) {
		this.energyUsage = energyUsage;
	}

	public void setActionPoints(int actionPoints) {
		this.actionPoints = actionPoints;
	}

	public void setIgnorePoints(int ignorePoints) {
		this.ignorePoints = ignorePoints;
	}

	public String getShortDescription() {
		return shortDescription;
	}

	public int getEnergyUsage() {
		return energyUsage;
	}

	public int getActionPoints() {
		return actionPoints;
	}

	public int getIgnorePoints() {
		return ignorePoints;
	}

	public int getMoneyReward() {
		return moneyReward;
	}

	public void setMoneyReward(int moneyReward) {
		this.moneyReward = moneyReward;
	}

}
