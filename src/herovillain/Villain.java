package herovillain;

public class Villain extends Character {

	public Villain(String name) {
		super(name);
		setStatus(Status.VILLAIN);
		setActions(40);
	}

}
