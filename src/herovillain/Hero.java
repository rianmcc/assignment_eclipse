package herovillain;

public class Hero extends Character {

	public Hero(String name) {
		super(name);
		setStatus(Status.HERO);
		setActions(60);
	}

	public Hero(String name, int energy, int actions, int money, int maxEnergy) {
		super(name, energy, actions, money, maxEnergy);
	}

}
