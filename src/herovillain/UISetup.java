/*
 Finish layout of main game
 Add music to intro screen
 Finish shop
 Finish difficulty levels
 Add money to game
 Add money to ui
 Action and energy bar shouldn't go over 100
 */
package herovillain;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Random;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.JRadioButton;
import javax.swing.JToggleButton;
import javax.swing.Timer;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JComboBox;

public class UISetup {

	// reference to application window
	private JFrame window;

	// Declare variables for JComponents for startScreenPnl
	private JButton newGameBtn;
	private JButton loadGameBtn;
	private JTextField playerNameTxt;
	private JRadioButton villainRadio;
	private JRadioButton heroRadio;
	private ButtonGroup characterTypeGroup;
	@SuppressWarnings("rawtypes")
	private JComboBox loadGameCmb;
	private JRadioButton easyRadio;
	private JRadioButton normalRadio;
	private JRadioButton hardRadio;
	private ButtonGroup difficultyGroup;

	// Declare variables for JComponents for mainGamePnl
	static JProgressBar energyBar;
	private JProgressBar actionBar;
	private JToggleButton sleepBtn;
	public static JLabel statusLbl;
	private JToggleButton pauseBtn;
	private JButton saveBtn;
	private JButton shopBtn;
	private JLabel nameLbl;
	private JLabel energyLbl;
	private JLabel actionsLbl;
	public static JLabel picLbl;
	private JLabel moneyLbl;

	// Declare class that will be instantiated to either Hero or Villain class
	public static Character player;

	// jpanel containers
	private JPanel startScreenPnl;
	private JPanel newGamePnl;
	private JPanel loadGamePnl;
	private JPanel mainGamePnl;

	int timeAfterLastCrime;

	// Timer
	Timer timer;

	// Crime array
	static Crime[] crimes;

	// Random variable for deciding if a crime should occur
	Random rand;

	String[] options;
	String[] notEnoughEnergyOptions;

	public static ShopItem[] shopItems;

	// Constructor used to setup GUI Components
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public UISetup() {
		/*
		 * Load save games from saves.txt and insert them into combo box so user
		 * can select which one they would like to load.
		 */
		Path path = Paths.get("saves.txt");
		File file = path.toFile();
		loadGameCmb = new JComboBox();
		final ArrayList<String> games = new ArrayList<String>();
		try (BufferedReader in = new BufferedReader(new FileReader(file))) {
			String line;
			while ((line = in.readLine()) != null) {
				games.add(line);
			}
		} catch (IOException e) {
			System.out.print(e);
		}
		if (!games.isEmpty()) {
			String[] cmbItems = new String[games.size()];
			for (int i = 0; i < games.size(); i++) {
				String[] line = games.get(i).split("\t");
				cmbItems[i] = line[0] + " | " + line[line.length - 1];
			}
			loadGameCmb = new JComboBox(cmbItems);
		}

		crimes = new Crime[6];
		crimes[0] = new Crime("Bank robbery", 20, 20, 10, 60);
		crimes[1] = new Crime("Lorry hijacking", 10, 15, 5, 15);
		crimes[2] = new Crime("Prison riot", 20, 30, 10, 20);
		crimes[3] = new Crime("Armed Shop robbery", 10, 10, 3, 20);
		crimes[4] = new Crime("Hostage situation", 20, 15, 10, 10);
		crimes[5] = new Crime("Goverment assassination plot", 30, 50, 20, 30);
		options = new String[] { "Commit crime", "Stop crime", "Ignore" };
		notEnoughEnergyOptions = new String[] { "Ignore" };

		// Setup shop items
		shopItems = new ShopItem[6];
		shopItems[0] = new ShopItem("Armor", 200, new ImageIcon(getClass()
				.getClassLoader().getResource("body_armor.jpg")),
				"This high quality armor will increase your maximum energy by 50 points.");
		shopItems[1] = new ShopItem("Lucky hat", 200, new ImageIcon(getClass()
				.getClassLoader().getResource("lucky_hat.png")),
				"This lucky hat will get you a bigger money award from events"
						+ " in the game");
		shopItems[2] = new ShopItem("Gun", 100, new ImageIcon(getClass()
				.getClassLoader().getResource("gun.jpg")),
				"Recommended for villains. This 9mm pistol will decrease action points "
						+ " more for each event");
		shopItems[3] = new ShopItem(
				"Blow darts",
				100,
				new ImageIcon(getClass().getClassLoader().getResource(
						"blow_dart.jpg")),
				"Recommended for heros. Take down enemies silently. Increases action points for each event.");
		shopItems[4] = new ShopItem("Small food", 30, new ImageIcon(getClass()
				.getClassLoader().getResource("small_food.jpg")),
				"\nRestore 30 energy with this plain burger.");
		shopItems[5] = new ShopItem("Large food", 100, new ImageIcon(getClass()
				.getClassLoader().getResource("large_food.jpg")),
				"\nRestore all of your energy with this large burger.");

		// create new window with title
		window = new JFrame("Hero's & Villain's");

		// create JPanels. One for the initial game screen and one for main game
		startScreenPnl = new JPanel();
		mainGamePnl = new JPanel(new GridBagLayout());
		loadGamePnl = new JPanel(new GridBagLayout());
		loadGamePnl.setBorder(BorderFactory.createTitledBorder("Load Game"));
		newGamePnl = new JPanel(new GridBagLayout());
		newGamePnl.setBorder(BorderFactory.createTitledBorder("New Game"));

		GridBagConstraints c = new GridBagConstraints();

		// Add components for the initial screen to startScreenPnl
		newGameBtn = new JButton("New game");
		loadGameBtn = new JButton("Load game");
		playerNameTxt = new JTextField(20);

		heroRadio = new JRadioButton("Hero");
		villainRadio = new JRadioButton("Villain");
		characterTypeGroup = new ButtonGroup();
		characterTypeGroup.add(heroRadio);
		characterTypeGroup.add(villainRadio);

		easyRadio = new JRadioButton("Easy");
		normalRadio = new JRadioButton("Normal");
		hardRadio = new JRadioButton("Hard");
		difficultyGroup = new ButtonGroup();
		difficultyGroup.add(easyRadio);
		difficultyGroup.add(normalRadio);
		difficultyGroup.add(hardRadio);

		c.insets = new Insets(10, 10, 10, 10);
		c.gridx = 0;
		c.gridy = 0;
		loadGamePnl.add(loadGameCmb, c);
		c.gridx = 1;
		c.gridy = 0;
		loadGamePnl.add(loadGameBtn, c);
		c.insets = new Insets(5, 5, 5, 5);
		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 3;
		newGamePnl.add(playerNameTxt, c);
		c.gridwidth = 1;
		c.gridx = 0;
		c.gridy = 1;
		newGamePnl.add(heroRadio, c);
		c.gridx = 2;
		c.gridy = 1;
		newGamePnl.add(villainRadio, c);
		c.gridx = 0;
		c.gridy = 2;
		newGamePnl.add(easyRadio, c);
		c.gridx = 1;
		c.gridy = 2;
		newGamePnl.add(normalRadio, c);
		c.gridx = 2;
		c.gridy = 2;
		newGamePnl.add(hardRadio, c);
		c.gridx = 1;
		c.gridy = 3;
		newGamePnl.add(newGameBtn, c);

		// Add startScreenPnl to window because it is the first screen to show
		startScreenPnl.add(loadGamePnl);
		startScreenPnl.add(newGamePnl);
		window.add(startScreenPnl);

		energyBar = new JProgressBar(0, 100);
		energyBar.setStringPainted(true);
		actionBar = new JProgressBar(0, 100);
		actionBar.setStringPainted(true);
		sleepBtn = new JToggleButton("Sleep");
		pauseBtn = new JToggleButton("Pause");
		statusLbl = new JLabel();
		statusLbl.setBackground(Color.yellow);
		statusLbl.setOpaque(true);
		picLbl = new JLabel();
		saveBtn = new JButton("Save Game");
		shopBtn = new JButton("Shop");
		nameLbl = new JLabel();
		energyLbl = new JLabel("Energy remaining:");
		actionsLbl = new JLabel("Actions bar");
		moneyLbl = new JLabel("Money: ");

		// Add components for main game to mainGamePnl
		c.fill = GridBagConstraints.BOTH;
		c.gridwidth = 1;
		c.gridheight = 1;
		c.gridx = 0;
		c.gridy = 0;
		mainGamePnl.add(nameLbl, c);
		c.gridwidth = 6;
		c.gridheight = 1;
		c.gridx = 2;
		c.gridy = 0;
		mainGamePnl.add(statusLbl, c);
		c.gridwidth = 5;
		c.gridheight = 10;
		c.gridx = 0;
		c.gridy = 1;
		mainGamePnl.add(picLbl, c);
		c.gridwidth = 3;
		c.gridheight = 1;
		c.gridx = 2;
		c.gridy = 3;
		mainGamePnl.add(energyLbl, c);
		c.gridwidth = 3;
		c.gridheight = 1;
		c.gridx = 2;
		c.gridy = 4;
		mainGamePnl.add(energyBar, c);
		c.gridwidth = 3;
		c.gridheight = 1;
		c.gridx = 2;
		c.gridy = 5;
		mainGamePnl.add(actionsLbl, c);
		c.gridwidth = 3;
		c.gridheight = 1;
		c.gridx = 2;
		c.gridy = 6;
		mainGamePnl.add(actionBar, c);
		c.gridwidth = 3;
		c.gridheight = 1;
		c.gridx = 2;
		c.gridy = 7;
		mainGamePnl.add(moneyLbl, c);
		c.gridwidth = 1;
		c.gridheight = 1;
		c.gridx = 0;
		c.gridy = 11;
		mainGamePnl.add(sleepBtn, c);
		c.gridwidth = 1;
		c.gridheight = 1;
		c.gridx = 1;
		c.gridy = 11;
		mainGamePnl.add(pauseBtn, c);
		c.gridwidth = 1;
		c.gridheight = 1;
		c.gridx = 2;
		c.gridy = 11;
		mainGamePnl.add(shopBtn, c);
		c.gridwidth = 1;
		c.gridheight = 1;
		c.gridx = 3;
		c.gridy = 11;
		mainGamePnl.add(saveBtn, c);

		// set size of window
		window.setMinimumSize(new Dimension(450, 270));
		// set how the window deals with an on close
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// set the window to be shown and non-resizable
		window.setResizable(false);
		window.setVisible(true);

		// Timer fires every 1000ms (1 Second)
		rand = new Random();
		timer = new Timer(1000, null);
		timer.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent ae) {
				timeAfterLastCrime++;
				if (player.getEnergy() <= 0) {
					timer.stop();
					player.setStatus(Status.HUMAN);
					picLbl.setIcon(player.images[2]);
					statusLbl.setText(player.getStatus().toString());
					statusLbl.setBackground(Color.GRAY);
					JOptionPane.showMessageDialog(window, "Game over!");
					window.dispose();
				}
				if (sleepBtn.isSelected()) {
					player.addEnergy(10);
				} else {
					player.subtractEnergy(2);
				}
				// The times that crimes happen will be random but also won't
				// happen too close together (within 3 seconds).
				if (rand.nextInt(4) == 0 && timeAfterLastCrime > 3) {
					generateCrime();
					// Reset timer
					timeAfterLastCrime = 0;
				}
				updateUI();
			}
		});

		newGameBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				if (playerNameTxt.getText().isEmpty()
						|| characterTypeGroup.getSelection() == null
						|| difficultyGroup.getSelection() == null) {
					JOptionPane
							.showMessageDialog(
									window,
									"You must enter a name, "
											+ "select a difficulty and select a character type!");
				} else {
					if (heroRadio.isSelected()) {
						player = new Hero(playerNameTxt.getText());
					} else if (villainRadio.isSelected()) {
						player = new Villain(playerNameTxt.getText());
					}
					if (easyRadio.isSelected()) {
						setDifficulty(0);
					} else if (normalRadio.isSelected()) {
						setDifficulty(1);
					} else if (hardRadio.isSelected()) {
						setDifficulty(2);
					}
					// Get rid of the start screen and display the main game
					loadImages();
					updateUI();
					nameLbl.setText(player.getName());
					startScreenPnl.setVisible(false);
					window.remove(startScreenPnl);
					window.setMinimumSize(new Dimension(400, 300));
					window.add(mainGamePnl);
					timer.start();

				}
			}
		});

		pauseBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent ae) {
				if (pauseBtn.isSelected()) {
					timer.stop();
				} else {
					timer.start();
				}
			}
		});

		saveBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent ae) {
				try {
					doSave();
				} catch (IOException ex) {
					Logger.getLogger(UISetup.class.getName()).log(Level.SEVERE,
							null, ex);
				}
			}
		});

		loadGameBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent ae) {
				if (!games.isEmpty()) {
					int index = loadGameCmb.getSelectedIndex();
					String line = games.get(index);
					String[] split = line.split("\t");
					player = new Hero(split[0], Integer.parseInt(split[1]),
							Integer.parseInt(split[2]), Integer
									.parseInt(split[4]), Integer
									.parseInt(split[5]));
					switch (split[3]) {
					case "EASY":
						setDifficulty(0);
						break;
					case "NORMAL":
						setDifficulty(1);
						break;
					case "HARD":
						setDifficulty(2);
						break;
					}
					energyBar.setMaximum(player.getMaxEnergy());
					// Get rid of the start screen and display the main game
					loadImages();
					updateUI();
					nameLbl.setText(player.getName());
					startScreenPnl.setVisible(false);
					window.remove(startScreenPnl);
					window.setMinimumSize(new Dimension(400, 300));
					window.add(mainGamePnl);
					timer.start();
				} else {
					JOptionPane.showMessageDialog(window, "There are no saved "
							+ "games! You must start a new game");
				}
			}
		});

		shopBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				pauseBtn.setSelected(true);
				timer.stop();
				Runnable runner = new Runnable() {
					public void run() {
						new ShopGUI();
					}
				};
				EventQueue.invokeLater(runner);
			}
		});

	}

	/**
	 * Updates the values of the players energy and actions based on their
	 * choice in the crime dialog option.
	 *
	 * @param randCrime
	 *            The randomly selected Crime object
	 * @param optionSelected
	 *            The option the user selected (commit, stop, ignore).
	 */
	public void processCrime(Crime randCrime, int optionSelected) {
		if (optionSelected == 0) {
			player.subtractEnergy(randCrime.getEnergyUsage());
			player.subtractActions(randCrime.getActionPoints());
			player.addMoney(randCrime.getMoneyReward());
			// Wake up player if they are sleeping
			sleepBtn.setSelected(false);
		} else if (optionSelected == 1) {
			player.subtractEnergy(randCrime.getEnergyUsage());
			player.addActions(randCrime.getActionPoints());
			player.addMoney(randCrime.getMoneyReward());
			// Wake up player if they are sleeping
			sleepBtn.setSelected(false);
		} else if (optionSelected == 2) {
			if (player.getActions() < 50) {
				player.addActions(randCrime.getIgnorePoints());
			} else {
				player.subtractActions(randCrime.getIgnorePoints());
			}
		}
		player.updateStatus();
	}

	/**
	 * Updates the energy and action progress bar and the status bar on the UI.
	 */
	public void updateUI() {
		energyBar.setValue(player.getEnergy());
		actionBar.setValue(player.getActions());
		player.updateStatus();
		statusLbl.setText(player.getStatus().toString());
		moneyLbl.setText("Money: " + player.getMoney());
	}

	public void loadImages() {
		player.images = new ImageIcon[5];
		player.images[0] = new ImageIcon(getClass().getClassLoader()
				.getResource("superhero.jpg"));
		player.images[1] = new ImageIcon(getClass().getClassLoader()
				.getResource("hero.jpg"));
		player.images[2] = new ImageIcon(getClass().getClassLoader()
				.getResource("human.jpg"));
		player.images[3] = new ImageIcon(getClass().getClassLoader()
				.getResource("villain.jpg"));
		player.images[4] = new ImageIcon(getClass().getClassLoader()
				.getResource("supervillain.jpg"));
	}

	/**
	 * Randomly picks a crime and creates a option dialog so the user can make
	 * their choice (commit, stop, ignore). Then calls processCrime() with the
	 * users choice.
	 */
	public void generateCrime() {
		final Crime randCrime = crimes[rand.nextInt(crimes.length)];
		if (player.getEnergy() <= randCrime.getEnergyUsage()) {
			JOptionPane.showOptionDialog(
					window,
					randCrime.getShortDescription() + "\n Will use "
							+ randCrime.getEnergyUsage() + " energy."
							+ "\n Will change action bar by "
							+ randCrime.getActionPoints() + " points."
							+ "\n If ignored, will change action bar by "
							+ randCrime.getIgnorePoints() + "\n Earns "
							+ randCrime.getMoneyReward() + " money.", "Event",
					JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE,
					null, notEnoughEnergyOptions, notEnoughEnergyOptions[0]);
			processCrime(randCrime, 2);
		} else {
			int optionSelected = JOptionPane.showOptionDialog(
					window,
					randCrime.getShortDescription() + "\n Will use "
							+ randCrime.getEnergyUsage() + " energy."
							+ "\n Will change action bar by "
							+ randCrime.getActionPoints() + " points."
							+ "\n If ignored, will change action bar by "
							+ randCrime.getIgnorePoints() + "\n Earns "
							+ randCrime.getMoneyReward() + " money.", "Event",
					JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE,
					null, options, options[2]);
			processCrime(randCrime, optionSelected);
		}

	}

	/**
	 * Write current player information (name, energy, actions) and current time
	 * to saves.txt. Appends information as a new line.
	 *
	 * @throws java.io.IOException
	 */
	public void doSave() throws IOException {
		FileWriter out = new FileWriter("saves.txt", true);
		DateFormat df = DateFormat.getDateTimeInstance(DateFormat.MEDIUM,
				DateFormat.MEDIUM);
		String formattedDate = df.format(new Date());
		out.write(player.getName() + "\t" + player.getEnergy() + "\t"
				+ player.getActions() + "\t"
				+ player.getDifficulty().toString() + "\t" + player.getMoney()
				+ "\t" + player.getMaxEnergy() + "\t" + formattedDate + "\n");
		out.close();
	}

	public void setDifficulty(int difficulty) {
		switch (difficulty) {
		case 0:
			player.setDifficulty(Difficulty.EASY);
			for (Crime crime : crimes) {
				crime.setActionPoints((int) (crime.getActionPoints() + (crime
						.getActionPoints() * .2)));
				crime.setEnergyUsage((int) (crime.getEnergyUsage() - (crime
						.getEnergyUsage() * .2)));
				crime.setMoneyReward((int) (crime.getMoneyReward() + (crime
						.getMoneyReward() * .2)));
			}
			break;
		case 1:
			player.setDifficulty(Difficulty.NORMAL);
			break;
		case 2:
			player.setDifficulty(Difficulty.HARD);
			for (Crime crime : crimes) {
				crime.setActionPoints((int) (crime.getActionPoints() - (crime
						.getActionPoints() * .2)));
				crime.setEnergyUsage((int) (crime.getEnergyUsage() + (crime
						.getEnergyUsage() * .2)));
				crime.setMoneyReward((int) (crime.getMoneyReward() - (crime
						.getMoneyReward() * .2)));
			}
			break;
		}
	}
}
