package herovillain;

import javax.swing.ImageIcon;

public class ShopItem {
	private String name;
	private int cost;
	private ImageIcon image;
	private String description;

	public ShopItem(String name, int cost, ImageIcon image, String description) {
		this.name = name;
		this.cost = cost;
		this.image = image;
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public ImageIcon getImage() {
		return image;
	}

	public void setImage(ImageIcon image) {
		this.image = image;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
