package herovillain;

import java.awt.Color;
import javax.swing.ImageIcon;

public abstract class Character {

	private String name;
	private Status status;
	private Difficulty difficulty;

	private int actions;
	private int money = 50;
	// All players energy will start at 50
	private int energy = 50;
	private int maxEnergy = 100;
	ImageIcon[] images;

	public Character(String name) {
		this.name = name;
	}

	public Character(String name, int energy, int actions, int money,
			int maxEnergy) {
		this.name = name;
		this.actions = actions;
		this.energy = energy;
		this.money = money;
		this.maxEnergy = maxEnergy;
	}

	public int getMaxEnergy() {
		return maxEnergy;
	}

	public void setMaxEnergy(int maxEnergy) {
		this.maxEnergy = maxEnergy;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getEnergy() {
		return energy;
	}

	public void setEnergy(int energy) {
		this.energy = energy;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public int getActions() {
		return actions;
	}

	public void setActions(int actions) {
		this.actions = actions;
	}

	public Difficulty getDifficulty() {
		return difficulty;
	}

	public void setDifficulty(Difficulty difficulty) {
		this.difficulty = difficulty;
	}

	public int getMoney() {
		return money;
	}

	public void setMoney(int money) {
		this.money = money;
	}

	/**
	 * This is a shortcut function to make subtracting energy more readable.
	 */
	public void subtractEnergy(int amount) {
		energy -= amount;
	}

	/**
	 * This is a shortcut function to make adding energy more readable.
	 */
	public void addEnergy(int amount) {
		energy += amount;
		if (energy > maxEnergy)
			energy = maxEnergy;
	}

	/**
	 * This is a shortcut function to make subtracting actions more readable.
	 */
	public void subtractActions(int amount) {
		actions -= amount;
		if (actions < 0)
			actions = 0;
	}

	/**
	 * This is a shortcut function to make adding actions more readable.
	 */
	public void addActions(int amount) {
		actions += amount;
		if (actions > 100)
			actions = 100;
	}

	/**
	 * This is a shortcut function to make subtracting money more readable.
	 */
	public void subtractMoney(int amount) {
		money -= amount;
	}

	/**
	 * This is a shortcut function to make adding money more readable.
	 */
	public void addMoney(int amount) {
		money += amount;
	}

	/**
	 * This function will update the status of the player based on their action
	 * value.
	 */
	public void updateStatus() {

		if (actions > 89) {
			status = Status.SUPERHERO;
			UISetup.picLbl.setIcon(images[0]);
			UISetup.statusLbl.setBackground(Color.GREEN);
		} else if (actions > 59) {
			status = Status.HERO;
			UISetup.picLbl.setIcon(images[1]);
			UISetup.statusLbl.setBackground(Color.GREEN);
		} else if (actions > 40) {
			status = Status.HUMAN;
			UISetup.picLbl.setIcon(images[2]);
			UISetup.statusLbl.setBackground(Color.GRAY);
		} else if (actions > 10) {
			status = Status.VILLAIN;
			UISetup.picLbl.setIcon(images[3]);
			UISetup.statusLbl.setBackground(Color.RED);
		} else {
			status = Status.SUPERVILLAIN;
			UISetup.picLbl.setIcon(images[4]);
			UISetup.statusLbl.setBackground(Color.RED);
		}
	}

}
