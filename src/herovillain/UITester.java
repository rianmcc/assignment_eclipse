package herovillain;

import java.awt.EventQueue;

public class UITester {

	public static void main(String[] args) {
		Runnable runner = new Runnable() {
			public void run() {
				new UISetup();
			}
		};
		EventQueue.invokeLater(runner);
	}
}
