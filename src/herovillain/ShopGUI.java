package herovillain;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class ShopGUI {
	private JFrame shopWindow;

	private JPanel mainPanel;

	private JButton buyBtn;
	private JButton nextBtn;
	private JButton previousBtn;
	private JLabel imageLbl;
	private JTextArea descriptionLbl;
	private JLabel nameLbl;
	private JLabel costLbl;

	private ShopItem currentItem;
	private int itemCounter;

	public ShopGUI() {
		shopWindow = new JFrame("Shop");
		mainPanel = new JPanel(new GridBagLayout());

		// Set the first item in the array to currentItem.
		// The counter is used to keep track of what index in the array
		// currentItem is on.
		currentItem = UISetup.shopItems[0];
		itemCounter = 0;

		// Create all of the components for the shop
		buyBtn = new JButton("Buy item");
		imageLbl = new JLabel(currentItem.getImage());
		imageLbl.setHorizontalAlignment(JLabel.CENTER);
		descriptionLbl = new JTextArea(currentItem.getDescription());
		descriptionLbl.setEditable(false);
		descriptionLbl.setLineWrap(true);
		nameLbl = new JLabel(currentItem.getName());
		nameLbl.setHorizontalAlignment(JLabel.CENTER);
		costLbl = new JLabel("Cost: " + currentItem.getCost());
		costLbl.setHorizontalAlignment(JLabel.CENTER);
		nextBtn = new JButton("Next Item");
		previousBtn = new JButton("Previous Item");
		// The first item is default so you can't go to a previous item
		previousBtn.setEnabled(false);

		// Set the layout of all the components in mainPanel
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.insets = new Insets(3, 3, 3, 3);
		c.gridwidth = 1;
		c.gridheight = 1;
		c.gridx = 0;
		c.gridy = 0;
		mainPanel.add(previousBtn, c);
		c.gridwidth = 1;
		c.gridheight = 1;
		c.gridx = 2;
		c.gridy = 0;
		mainPanel.add(nextBtn, c);
		c.gridwidth = 1;
		c.gridheight = 1;
		c.gridx = 1;
		c.gridy = 1;
		mainPanel.add(nameLbl, c);
		c.gridwidth = 3;
		c.gridheight = 1;
		c.gridx = 0;
		c.gridy = 2;
		mainPanel.add(imageLbl, c);
		c.gridwidth = 3;
		c.gridheight = 1;
		c.gridx = 0;
		c.gridy = 3;
		mainPanel.add(descriptionLbl, c);
		c.gridwidth = 3;
		c.gridheight = 1;
		c.gridx = 0;
		c.gridy = 4;
		mainPanel.add(costLbl, c);
		c.gridwidth = 3;
		c.gridheight = 1;
		c.gridx = 0;
		c.gridy = 5;
		mainPanel.add(buyBtn, c);

		shopWindow.add(mainPanel);

		shopWindow.setMinimumSize(new Dimension(400, 400));
		// set how the window deals with an on close
		shopWindow.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		// set the window to be shown and non-resizable
		shopWindow.setResizable(false);
		shopWindow.setVisible(true);

		nextBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				previousBtn.setEnabled(true);
				itemCounter++;
				currentItem = UISetup.shopItems[itemCounter];
				imageLbl.setIcon(currentItem.getImage());
				descriptionLbl.setText(currentItem.getDescription());
				nameLbl.setText(currentItem.getName());
				costLbl.setText("Cost: " + currentItem.getCost());
				if (itemCounter == UISetup.shopItems.length - 1) {
					nextBtn.setEnabled(false);
				}
			}
		});

		previousBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				nextBtn.setEnabled(true);
				itemCounter--;
				currentItem = UISetup.shopItems[itemCounter];
				imageLbl.setIcon(currentItem.getImage());
				descriptionLbl.setText(currentItem.getDescription());
				nameLbl.setText(currentItem.getName());
				costLbl.setText("Cost: " + currentItem.getCost());
				if (itemCounter == 0) {
					previousBtn.setEnabled(false);
				}
			}
		});

		buyBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// Perform actions based no what itemCounter is when buyBtn is
				// clicked.
				// This will mean that the correct item is bought.
				switch (itemCounter) {
				case 0:
					if (UISetup.player.getMoney() >= UISetup.shopItems[0]
							.getCost()) {
						UISetup.player.setMaxEnergy(UISetup.player
								.getMaxEnergy() + 50);
						UISetup.energyBar.setMaximum(UISetup.player
								.getMaxEnergy());
						UISetup.player.setEnergy(UISetup.player.getMaxEnergy());
						// Remove money from player
						UISetup.player.subtractMoney(UISetup.shopItems[0]
								.getCost());
					} else {
						JOptionPane
								.showMessageDialog(shopWindow,
										"You can't afford this! You only have "
												+ UISetup.player.getMoney()
												+ " coins.");
					}
					break;
				case 1:
					if (UISetup.player.getMoney() >= UISetup.shopItems[1]
							.getCost()) {
						for (Crime crime : UISetup.crimes) {
							crime.setMoneyReward((int) (crime.getMoneyReward() + (crime
									.getMoneyReward() * .1)));
						}
						// Remove money from player
						UISetup.player.subtractMoney(UISetup.shopItems[1]
								.getCost());
					} else {
						JOptionPane
								.showMessageDialog(shopWindow,
										"You can't afford this! You only have "
												+ UISetup.player.getMoney()
												+ " coins.");
					}
					break;
				case 2:
					if (UISetup.player.getMoney() >= UISetup.shopItems[2]
							.getCost()) {
						for (Crime crime : UISetup.crimes) {
							crime.setActionPoints((int) (crime
									.getActionPoints() - (crime
									.getActionPoints() * .1)));
						}
						// Remove money from player
						UISetup.player.subtractMoney(UISetup.shopItems[2]
								.getCost());
					} else {
						JOptionPane
								.showMessageDialog(shopWindow,
										"You can't afford this! You only have "
												+ UISetup.player.getMoney()
												+ " coins.");
					}
					break;
				case 3:
					if (UISetup.player.getMoney() >= UISetup.shopItems[3]
							.getCost()) {
						for (Crime crime : UISetup.crimes) {
							crime.setActionPoints((int) (crime
									.getActionPoints() + (crime
									.getActionPoints() * .1)));
						}
						// Remove money from player
						UISetup.player.subtractMoney(UISetup.shopItems[3]
								.getCost());
					} else {
						JOptionPane
								.showMessageDialog(shopWindow,
										"You can't afford this! You only have "
												+ UISetup.player.getMoney()
												+ " coins.");
					}
					break;
				case 4:
					if (UISetup.player.getMoney() >= UISetup.shopItems[4]
							.getCost()) {
						UISetup.player.addEnergy(30);
						// Remove money from player
						UISetup.player.subtractMoney(UISetup.shopItems[4]
								.getCost());
					} else {
						JOptionPane
								.showMessageDialog(shopWindow,
										"You can't afford this! You only have "
												+ UISetup.player.getMoney()
												+ " coins.");
					}
					break;
				case 5:
					if (UISetup.player.getMoney() >= UISetup.shopItems[5]
							.getCost()) {
						UISetup.player.setEnergy(UISetup.player.getMaxEnergy());
						// Remove money from player
						UISetup.player.subtractMoney(UISetup.shopItems[5]
								.getCost());
					} else {
						JOptionPane
								.showMessageDialog(shopWindow,
										"You can't afford this! You only have "
												+ UISetup.player.getMoney()
												+ " coins.");
					}
					break;
				}
			}
		});

	}
}
